const express = require('express');
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();
const app = express();
app.use(express.json());

// Author endpoints
app.post('/author', async (req, res) => {
    const { name, email, biografy } = req.body;
    try {
        const author = await prisma.author.create({
            data: {
                name,
                email,
                biografy,
            },
        });
        res.json(author);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

app.get('/authors', async (req, res) => {
    const authors = await prisma.author.findMany();
    res.json(authors);
});

// Coach endpoints
app.post('/coach', async (req, res) => {
    const { coachName, email, phoneNumber, courseId } = req.body;
    try {
        const coach = await prisma.coach.create({
            data: {
                coachName,
                email,
                phoneNumber,
                courseId,
            },
        });
        res.json(coach);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

app.get('/coaches', async (req, res) => {
    const coaches = await prisma.coach.findMany();
    res.json(coaches);
});

// Course endpoints
app.post('/course', async (req, res) => {
    const { courseName, description, startDate, endDate } = req.body;
    try {
        const course = await prisma.course.create({
            data: {
                courseName,
                description,
                startDate,
                endDate,
            },
        });
        res.json(course);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

app.get('/courses', async (req, res) => {
    const courses = await prisma.course.findMany();
    res.json(courses);
});

// Topic endpoints
app.post('/topic', async (req, res) => {
    const { topicName, description, duration, courseId } = req.body;
    try {
        const topic = await prisma.topic.create({
            data: {
                topicName,
                description,
                duration,
                courseId,
            },
        });
        res.json(topic);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

app.get('/topics', async (req, res) => {
    const topics = await prisma.topic.findMany();
    res.json(topics);
});

// Schedule endpoints
app.post('/schedule', async (req, res) => {
    const { coachId, topicId, scheduleTime, scheduleDay, scheduleLocation } = req.body;
    try {
        const schedule = await prisma.schedule.create({
            data: {
                coachId,
                topicId,
                scheduleTime,
                scheduleDay,
                scheduleLocation,
            },
        });
        res.json(schedule);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

app.get('/schedules', async (req, res) => {
    const schedules = await prisma.schedule.findMany();
    res.json(schedules);
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});

process.on('SIGINT', async () => {
    await prisma.$disconnect();
    process.exit();
});