/*
  Warnings:

  - You are about to alter the column `deletedAt` on the `Coach` table. The data in that column could be lost. The data in that column will be cast from `DateTime(0)` to `DateTime`.
  - You are about to alter the column `deletedAt` on the `CoachTopic` table. The data in that column could be lost. The data in that column will be cast from `DateTime(0)` to `DateTime`.
  - You are about to alter the column `deletedAt` on the `CoachTopicSchedule` table. The data in that column could be lost. The data in that column will be cast from `DateTime(0)` to `DateTime`.
  - You are about to alter the column `deletedAt` on the `Course` table. The data in that column could be lost. The data in that column will be cast from `DateTime(0)` to `DateTime`.
  - You are about to alter the column `deletedAt` on the `Schedule` table. The data in that column could be lost. The data in that column will be cast from `DateTime(0)` to `DateTime`.
  - You are about to alter the column `deletedAt` on the `Topic` table. The data in that column could be lost. The data in that column will be cast from `DateTime(0)` to `DateTime`.

*/
-- DropForeignKey
ALTER TABLE `Coach` DROP FOREIGN KEY `Coach_courseId_fkey`;

-- AlterTable
ALTER TABLE `Coach` MODIFY `deletedAt` DATETIME NULL;

-- AlterTable
ALTER TABLE `CoachTopic` MODIFY `deletedAt` DATETIME NULL;

-- AlterTable
ALTER TABLE `CoachTopicSchedule` MODIFY `deletedAt` DATETIME NULL;

-- AlterTable
ALTER TABLE `Course` MODIFY `deletedAt` DATETIME NULL;

-- AlterTable
ALTER TABLE `Schedule` MODIFY `deletedAt` DATETIME NULL;

-- AlterTable
ALTER TABLE `Topic` MODIFY `deletedAt` DATETIME NULL;
